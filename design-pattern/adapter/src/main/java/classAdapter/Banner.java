package classAdapter;

public class Banner {

    private String name;

    public Banner(String name) {
        this.name = name;
    }

    public void printWithA() {
        System.out.println("(" + name + ")");
    }

    public void printWithB() {
        System.out.println("*" + name + "*");
    }
}
