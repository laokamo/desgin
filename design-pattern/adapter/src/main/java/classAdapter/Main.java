package classAdapter;

public class Main {
    public static void main(String[] args) {
        Print banner = new PrintBanner("Hello World");
        banner.printStrong();
        banner.printWeak();
    }
}
