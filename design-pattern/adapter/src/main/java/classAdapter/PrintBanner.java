package classAdapter;

public class PrintBanner extends Banner implements Print {

    public PrintBanner(String name) {
        super(name);
    }

    @Override
    public void printWeak() {
        this.printWithA();
    }

    @Override
    public void printStrong() {
        this.printWithB();
    }
}
