package entrust;

/**
 * 对象
 */
public abstract class Print {

    abstract void printWeak();

    abstract void printStrong();

}
