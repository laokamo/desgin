package entrust;

/**
 * 适配者
 */
public class PrintBanner extends Print {
    /**
     * 被适配者
     */
    private Banner banner;

    public PrintBanner(String banner) {
        this.banner = new Banner(banner);
    }

    @Override
    public void printWeak() {
        banner.printWithA();
    }

    @Override
    public void printStrong() {
        banner.printWithB();
    }
}
