package entrust;

/**
 * 被适配者
 */
public class Banner {

    private String name;

    public Banner(String name) {
        this.name = name;
    }

    public void printWithA() {
        System.out.println("(" + name + ")");
    }

    public void printWithB() {
        System.out.println("*" + name + "*");
    }
}
