public class StringDisplay extends AbstractDisplay{
    private String ch;
    private int line;
    public StringDisplay(String ch) {
        this.ch = ch;
        this.line = ch.getBytes().length;
    }

    @Override
    void open() {
        printLine();
    }

    @Override
    void close() {
        printLine();
    }

    @Override
    void print() {
        System.out.println(ch);
    }

    public void printLine(){
        System.out.print("*");
        for (int i = 0; i < line; i++) {
            System.out.print("-");
        }
        System.out.println("*");
    }

}
