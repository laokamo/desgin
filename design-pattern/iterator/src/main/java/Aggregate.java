/**
 * 集合接口
 */
public interface Aggregate {

    public abstract Iterator iterator();
}
