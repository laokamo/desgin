/**
 * 迭代器接口
 */
public interface Iterator {

    /**
     * 是否还有下一个
     * @return
     */
    public abstract boolean hasNext();

    /**
     * 获取下一个
     * @return
     */
    public abstract Object next();
}
