import java.util.ArrayList;
import java.util.List;

/**
 * 集合具体实现
 */
public class BookShelf implements Aggregate {
    private List<Book> books;

    public BookShelf(int maxSize) {
        this.books = new ArrayList<>(maxSize);
        System.out.println("书架可以放" + maxSize + "本书");
    }

    //取书
    public Book getIndex(int index) {
        return books.get(index);
    }

    //加书
    public void append(String name) {
        books.add(new Book(name));
    }

    //数量
    public int getLength() {
        return books.size();
    }

    @Override
    public Iterator iterator() {
        return new BookShelfIterator(this);
    }
}
