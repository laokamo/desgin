public class Main {

    public static void main(String[] args) {
//        int[] arr = {1,2,3,4,5,6};
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println(arr[i]);
//        }
        BookShelf bookShelf = new BookShelf(4);
        bookShelf.append("A");
        bookShelf.append("B");
        bookShelf.append("C");
        bookShelf.append("D");
        bookShelf.append("E");
        Iterator iterator = bookShelf.iterator();
        while (iterator.hasNext()) {
            Book book = (Book) iterator.next();
            System.out.println(book.getName());
        }

    }
}
